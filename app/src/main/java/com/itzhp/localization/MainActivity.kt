package com.itzhp.localization

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var localizationHelper: LocalizationHelper
    lateinit var greetTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        greetTextView=findViewById(R.id.greet_text)
        lifecycleScope.launch {
            val string = localizationHelper.getString("GREETINGS", listOf("World"))
            string?.let { greetTextView.text=it }
        }
    }
}