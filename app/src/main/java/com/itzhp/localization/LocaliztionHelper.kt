package com.itzhp.localization

import android.util.Log.d
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.URL
import javax.inject.Inject

class LocalizationHelper @Inject constructor() {

    private var localizedStrings: Map<String, String>? = null
    private var input =""
    private val TAG="LocalizationHelper"
    private val validUrl="https://alphaskynet.com/sample.json"
    private val inValidUrl="https://raw.githubusercontent.com/Sangeethacygnvs/android_test/main/localisation.json"

    suspend fun getString(key: String, runtimeValues: List<String>): String {
        if (localizedStrings == null) {
            try {
                localizedStrings = withContext(Dispatchers.IO) {
                    val jsonString = URL(inValidUrl).readText()
                    Gson().fromJson(jsonString, object : TypeToken<Map<String, String>>() {}.type)
                }
            }catch (e:JsonParseException){
                d(TAG,e.message!!)
            }

        }
        return if(localizedStrings!=null && localizedStrings?.contains(key) == true) getVal(localizedStrings?.get(key).toString()).replace("\$t", runtimeValues.joinToString(" ")) else key
    }

    private fun getVal(valueToChange: String): String {
        this.input = valueToChange
        if (input.contains("\${")) {
            val regex = """\$\{(.*?)\}""".toRegex()
            val matches = regex.findAll(this.input)

            for (match in matches) {
                this.input = input.replace(match.groupValues[0], localizedStrings?.get(match.groupValues[1]).toString())
                getVal(this.input)
            }
        }
        return this.input
    }
}